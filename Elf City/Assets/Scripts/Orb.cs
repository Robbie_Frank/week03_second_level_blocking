﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Orb : MonoBehaviour
{
    [SerializeField] float floatSpeed = 1;
    private float startingY;
    [SerializeField] bool movingUp = true;
    // Start is called before the first frame update
    void Start()
    {
        startingY = transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        if (movingUp)
        {
            this.transform.position += new Vector3(0, floatSpeed * Time.deltaTime, 0);
            if (transform.position.y >= startingY + 1)
            {
                movingUp = false;
            }
        }
        else
        {
            transform.position -= new Vector3(0, floatSpeed * Time.deltaTime, 0);
            if (transform.position.y <= startingY - 1)
            {
                movingUp = true;
            }
        }
    }
}