﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    // Variables
    bool isClosed = true;
    float speed = 1;
    bool opening = false;
    bool closing = false;
    float initialPosition;
    bool inRange = false;
    // References
    [SerializeField] GameObject building;
    // Start is called before the first frame update
    void Start()
    {
        initialPosition = building.transform.rotation.eulerAngles.y;
    }
    // Update is called once per frame
    void Update()
    {
        if (opening)
        {
            Open();
            isClosed = false;
            opening = false;
        }
        if (closing)
        {
            Close();
            isClosed = true;
            closing = false;
        }
    }
    private void OnMouseDown()
    {
        if (isClosed && inRange)
        {
            opening = true;
        }
        if (!isClosed && inRange)
        {
            closing = true;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        inRange = true;
    }
    private void OnTriggerExit(Collider other)
    {
        inRange = false;
    }
    void Open()
    {
        Quaternion rot = Quaternion.AngleAxis(initialPosition - 90, Vector3.up);
        transform.rotation = Quaternion.Slerp(transform.rotation, rot, speed);
    }
    void Close()
    {
        Quaternion rot = Quaternion.AngleAxis(initialPosition, Vector3.up);
        transform.rotation = Quaternion.Slerp(transform.rotation, rot, speed);
    }
}