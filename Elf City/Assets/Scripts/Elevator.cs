﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevator : MonoBehaviour
{
    // Variables
    [SerializeField] float maxY = 25.75f;
    [SerializeField] float minY = 0;
    [SerializeField] float speed = 5f;
    private bool isPressed = false;
    private bool canUse = false;
    private bool isRaised = false;
    private bool isMovingUp = false;
    private bool isMovingDown = false;
    // References
    [SerializeField] GameObject platform;
    // Update is called once per frame
    void Update()
    {
        if (isMovingUp)
        {
            ElevatorUp();
        }
        if (isMovingDown)
        {
            ElevatorDown();
        }
    }
    private void OnMouseDown()
    {
        if (canUse && !isRaised)
        {
            isMovingUp = true;
            if (!isPressed)
            {
                this.gameObject.transform.Translate(Vector3.down * 0.25f);
                isPressed = true;
            }
        }
        else if (canUse && isRaised)
        {
            isMovingDown = true;
            if (isPressed)
            {
                this.gameObject.transform.Translate(Vector3.up * 0.25f);
                isPressed = false;
            }
        }
        else
        {
            print("You are too far away to interact with this");
        }
    }
    private void OnTriggerStay(Collider other)
    {
        canUse = true;
    }
    private void OnTriggerExit(Collider other)
    {
        canUse = false;
    }
    private void ElevatorUp()
    {
        platform.transform.Translate(Vector3.up * speed * Time.deltaTime);
        if (platform.transform.position.y >= maxY)
        {
            isRaised = true;
            isMovingUp = false;
        }
    }
    private void ElevatorDown()
    {
        platform.transform.Translate(Vector3.down * speed * Time.deltaTime);
        if (platform.transform.position.y <= minY)
        {
            isRaised = false;
            isMovingDown = false;
        }
    }
}