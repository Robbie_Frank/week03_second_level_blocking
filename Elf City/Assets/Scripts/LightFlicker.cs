﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightFlicker : MonoBehaviour
{
    // Variables
    [SerializeField] Light fire;
    float speed = 0.125f;
    float timer = 0;
    // Update is called once per frame
    void Update()
    {
        timer += 1 * Time.deltaTime;
        if (timer >= speed)
        {
            float flicker = Random.Range(4.0f, 6.0f);
            fire.intensity = flicker;
            timer = 0;
        }
    }
}